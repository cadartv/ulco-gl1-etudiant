# This is mydoc-md 

## License 
This project is under the MIT License [see LICENSE.txt](README.md)

## List 
- Item 1
- Item 2

## Table
| column1 | column2 | 
|---------|---------|
| foo     | bar     |
| toto    | tata    |  


## Code
```hs
main :: IO ()
main = putStrLn "hello"
```
## Quote
>Quote

## Image
![](https://i.redd.it/2wcie9exgr9z.png)

