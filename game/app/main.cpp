#include "../include/game/game.hpp"
#include <iostream>
#include <string>
#include <random>
using namespace std;

int main() {
    std::random_device r;
    std::default_random_engine e1(r());
    std::uniform_int_distribution<int> uniform_dist(1, 100);
    int nb_rand = uniform_dist(e1);

    std::cout << "Welcome to GuessGame !" << std::endl;
    std::cout << game(nb_rand) << std::endl;
    return 0;
}



