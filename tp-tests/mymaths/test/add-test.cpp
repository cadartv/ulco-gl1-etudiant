#include <catch2/catch.hpp>
#include <mymaths/add.hpp>

TEST_CASE( "test add2" ){
    REQUIRE( add2(40) == 42 );
}

TEST_CASE( "test addn" ){
    REQUIRE( addn(20, 22) == 42 );
}