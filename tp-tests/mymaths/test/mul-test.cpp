#include <catch2/catch.hpp>
#include <mymaths/mul.hpp>

TEST_CASE( "test mul2" ){
    REQUIRE( mul2(21) == 42 );
}

TEST_CASE( "test mul n" ){
    REQUIRE( muln(21, 2) == 42 );
}